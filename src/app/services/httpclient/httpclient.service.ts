import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  private serverUrl = "localhost";
  // private serverUrl = "192.168.30.173";

  constructor(private http:HttpClient) {   }

  getHttpOptions(){
    return {
        headers : new HttpHeaders({
            'content-type' : 'application/json',
            'Authorization' : localStorage.getItem('token')
        })
    }
  }

  hiveUsers() {
    return this.http.get('http://192.168.1.152/hive/admin/employee_start');
  }

  loginprocess(params) {
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/users/login', params);
  }

  //Inventory Controller
  getDamageLog(){
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/inventory/damagelogs');
  }

  getAvailabilityLog(){
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/inventory/availabilitylogs');
  }

  getStationLog(){
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/inventory/stationlogs');
  }

  getRoom(){
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/inventory/room');
  }
  
  getStation(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/inventory/station', params);
  }

  unassignStation(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/inventory/inventoryItem/',params);
  }

  getInventory(){
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/inventory/item');
  }

  postInventory(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/inventory/item', params);
  }

  //Equipment Controller
  getEquipment() {
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/equipment/equipment');
  }

  //Equipment Serial Controller
  getEquipSerial(params) {
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/equipmentserial/equipSerial/'+params);
  }

  getItems() {
    return this.http.get('http://'+this.serverUrl+'/inventorysystem/api/equipmentserial/item');
  }

  removeItem(params) {
    return this.http.delete('http://'+this.serverUrl+'/inventorysystem/api/equipmentserial/item/'+params);
  }

  addEquipment(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/equipmentserial/item',params);
  }

  updateEquipment(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/equipmentserial/item',params);
  }

  getStock(){
    return this.http.get('http://'+this.serverUrl+'/',this.getHttpOptions());
  }

  setLastLoggedIn(params){
    return this.http.post('http://'+this.serverUrl+'/inventorysystem/api/users/setLastLoggedIn', params);
  }
}
