import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './view/login/login.component';
import { DashboardComponent } from './view/dashboard/dashboard.component';
import { ItemListComponent } from './view/item-list/item-list.component';
import { StockComponent } from './view/stock/stock.component';
import { ReportComponent } from './view/report/report.component';
import { LogsComponent } from './view/logs/logs.component';
import { DamageLogComponent } from './view/damage-log/damage-log.component';
import { AvailabilityLogComponent } from './view/availability-log/availability-log.component';
import { StationLogComponent } from './view/station-log/station-log.component';
import { TemplateHeaderComponent } from './view/template-header/template-header.component';
import { TemplateFooterComponent } from './view/template-footer/template-footer.component';
import { TemplateSidenavComponent } from './view/template-sidenav/template-sidenav.component';
import { TemplateMainComponent } from './view/template-main/template-main.component';
import { PageNotFoundComponent } from './view/page-not-found/page-not-found.component';
import { ConditionComponent } from './view/condition/condition.component';
import { IndividualReportComponent } from './view/individual-report/individual-report.component';
import { ProfileComponent } from './view/profile/profile.component';
import { BarcodeComponent } from './view/barcode/barcode.component';

import { 
  BrowserAnimationsModule, 
  // NoopAnimationsModule
} from '@angular/platform-browser/animations';

import {  
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatFormFieldModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from "@angular/material";


import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './services/auth/auth.service';
import {
  AuthGuardService as AuthGuard
} from './services/auth-guard/auth-guard.service'; //for authorization


import { NgxSpinnerModule } from 'ngx-spinner';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ItemListComponent,
    StockComponent,
    ReportComponent,
    TemplateHeaderComponent,
    TemplateFooterComponent,
    TemplateSidenavComponent,
    PageNotFoundComponent,
    TemplateMainComponent,
    LogsComponent,
    ConditionComponent,
    IndividualReportComponent,
    ProfileComponent,
    BarcodeComponent,
    DamageLogComponent,
    AvailabilityLogComponent,
    StationLogComponent,
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    // NoopAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    NgxSpinnerModule
  ],
  providers: [
    JwtHelperService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
