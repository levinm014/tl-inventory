import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate  } from '@angular/router';

import {
  AuthGuardService as AuthGuard
} from './services/auth-guard/auth-guard.service'; //for authorization

import { LoginComponent } from './view/login/login.component';
import { DashboardComponent } from './view/dashboard/dashboard.component';
import { ItemListComponent } from './view/item-list/item-list.component';
import { StockComponent } from './view/stock/stock.component';
import { ReportComponent } from './view/report/report.component';
import { LogsComponent } from './view/logs/logs.component';
import { DamageLogComponent } from './view/damage-log/damage-log.component';
import { AvailabilityLogComponent } from './view/availability-log/availability-log.component';
import { StationLogComponent } from './view/station-log/station-log.component';
import { TemplateMainComponent } from './view/template-main/template-main.component';
import { PageNotFoundComponent } from './view/page-not-found/page-not-found.component';
import { ConditionComponent } from './view/condition/condition.component';
import { IndividualReportComponent } from './view/individual-report/individual-report.component';
import { ProfileComponent } from './view/profile/profile.component'
import { BarcodeComponent } from './view/barcode/barcode.component';;

const appRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'tl', component: TemplateMainComponent, canActivate: [AuthGuard], children: [
      { path: '',  component: DashboardComponent, outlet: 'content' },
      { path: 'dashboard', component: DashboardComponent, outlet: 'content' },
      { path: 'itemlist', component: ItemListComponent, outlet: 'content' },
      { path: 'logs', component: LogsComponent, outlet: 'content', children: [
        { path: '', component: DamageLogComponent, outlet: 'logs' },
        { path: 'damage-log', component: DamageLogComponent, outlet: 'logs' },
        { path: 'availability-log', component: AvailabilityLogComponent, outlet: 'logs' },
        { path: 'station-log', component: StationLogComponent, outlet: 'logs' },
        { path: '**', component: PageNotFoundComponent, outlet: 'logs' }
      ]},
      { path: 'barcode', component: BarcodeComponent, outlet: 'content' },
      { path: 'profile', component: ProfileComponent, outlet: 'content' },              //, canActivate: [AuthGuard] <--- put in the route's properties if JWT is configured properly.
      { path: 'reports', component: ReportComponent, outlet: 'content', children: [
        { path: '', component: IndividualReportComponent, outlet: 'reports' },
        { path: 'ir', component: IndividualReportComponent, outlet: 'reports' },
        { path: 'stock', component: StockComponent, outlet: 'reports' },
        { path: 'condition', component: ConditionComponent, outlet: 'reports' },
        { path: '**', component: PageNotFoundComponent, outlet: 'reports' }
      ]},
    { path: '**', component: PageNotFoundComponent, outlet: 'content' }
    ]},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(
    appRoutes,
    { enableTracing: false }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }


