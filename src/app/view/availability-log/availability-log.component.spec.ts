import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityLogComponent } from './availability-log.component';

describe('AvailabilityLogComponent', () => {
  let component: AvailabilityLogComponent;
  let fixture: ComponentFixture<AvailabilityLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
