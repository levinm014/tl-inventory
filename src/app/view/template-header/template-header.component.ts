import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClientService } from '../../services/httpclient/httpclient.service';

@Component({
  selector: 'app-template-header',
  templateUrl: './template-header.component.html',
  styleUrls: ['./template-header.component.scss']
})
export class TemplateHeaderComponent implements OnInit {
  userLogged: string;

  constructor(private router: Router, private httpservice: HttpClientService) { }

  ngOnInit() {
    const user = localStorage.getItem('username');
    this.userLogged = user.charAt(0).toUpperCase() + user.slice(1);
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['login']);
  }

}
