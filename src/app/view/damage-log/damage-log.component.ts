import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { HttpClientService } from '../../services/httpclient/httpclient.service';

export interface DamageLogInterface {
  id: string,
  inventory_id: string,
  equipment_id: string,
  station_id: string,
  date_time_log: string
}

@Component({
  selector: 'app-damage-log',
  templateUrl: './damage-log.component.html',
  styleUrls: ['./damage-log.component.scss']
})
export class DamageLogComponent implements OnInit {
	dataSource: any;

	displayedColumns: string[] = ['id', 'inventory_id', 'equipment_id', 'station_id', 'date_time_log'];
  

	@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private httpservice: HttpClientService) { }

  ngOnInit() {
  	this.httpservice.getDamageLog().subscribe(
      (response:any)=>{
      	this.dataSource = new MatTableDataSource<DamageLogInterface>(response);
      	this.dataSource.sort = this.sort;
      	this.dataSource.paginator = this.paginator;
      },
    	(error)=>{
      console.log(error);
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
