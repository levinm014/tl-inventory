import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageLogComponent } from './damage-log.component';

describe('DamageLogComponent', () => {
  let component: DamageLogComponent;
  let fixture: ComponentFixture<DamageLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
