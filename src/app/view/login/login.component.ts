import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgxSpinnerService  } from 'ngx-spinner';

import { HttpClientService } from '../../services/httpclient/httpclient.service';

export interface LoginInterface {
  token: string;
  userid: string;
  username: string;
  accounttype: string;
  date_created: string;
  last_logged_in: string;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  username: string;
  password: string;
  constructor(private httpservice: HttpClientService, private router: Router, private spinner: NgxSpinnerService) {}

  ngOnInit() {
  }

  login(){
    this.spinner.show();
    let params = {
      "username":this.username,
      "password":this.password
    }
    this.httpservice.loginprocess(params).subscribe(
      (response:LoginInterface)=>{
        localStorage.setItem('token',response.token);
        localStorage.setItem('username',response.username);
        localStorage.setItem('userid',response.userid);
        localStorage.setItem('date_created',response.date_created);
        localStorage.setItem('last_logged_in',response.last_logged_in);

        let current_date = new Date()
        let params = {
          "user": localStorage.getItem('username'),
          "current_timestamp": current_date.toString(),
        }
        this.httpservice.setLastLoggedIn(params).subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log(error);
          }
        )

        this.router.navigate(['tl',{outlets: {'content':'dashboard'}}]);
        this.spinner.hide();
      },
    (error)=>{
      console.log(error);
      this.spinner.hide();
    }
  )
  }
}
