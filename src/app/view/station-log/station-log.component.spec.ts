import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationLogComponent } from './station-log.component';

describe('StationLogComponent', () => {
  let component: StationLogComponent;
  let fixture: ComponentFixture<StationLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
