import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClientService } from 'src/app/services/httpclient/httpclient.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

declare function JsBarcode(param1,param2): any;

export interface ItemListInterface {
  id: string,
  specifications: string,
  equipment_id: string,
  status: string,
  serial: string
}

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})

export class ItemListComponent implements OnInit {
  dataSource: any;
  equipment: string;
  specs: string;
  serial: string;
  equipments: any;
  btnMsg: string;
  postAction: string;
  msg: string;
  itemlist;

  displayedColumns: string[] = ['serial', 'equipment_id', 'specifications', 'status', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  closeResult: string;
  constructor(private httpservice: HttpClientService, private modalService: NgbModal, private toaster: MatSnackBar) { }

  ngOnInit() {
    this.httpservice.getEquipment().subscribe(            //initialize
      (response) => {            
        console.log(response);                         //the list of equipments
        this.equipments = response;                       //for the dropdown
      },
      (error) => {
        console.log(error);
      }
    )

    this.getItems(); //Calls the fn to retrieve item list
  }

  generateBarcode(equipment){
    /* Generate number for barcode */
    let val = String(Math.floor(Math.random() * Math.floor(10 ** 9)))
    /* Show leading zero if number is less than 10 digits */
    while (val.length < 9) {
      val = "0" + val
    }

    let barcode = equipment+val;
    JsBarcode('#barcode',barcode);
    this.serial = barcode;
  }

  //Open Add Modal
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });       //to open ngModal

    this.equipment = "";
    this.specs = "";
    this.serial = "";
    JsBarcode('#barcode',"thinklogic");

    this.btnMsg ="Add";
    this.msg = "added"
    this.postAction = "insert";
  }

  getItems() {
    this.httpservice.getItems().subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource<ItemListInterface>(response);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  editEquipment(content, data){
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });       //to open ngModal
    document.getElementById("inputEquipment").setAttribute('disabled', 'true');
    this.equipment = data.equipment_id;
    this.specs = data.specifications;
    this.serial = data.serial;

    JsBarcode('#barcode',data.serial);

    this.btnMsg = "Update";
    this.postAction = "update";
    this.msg = "updated"
  }

  removeEquipment(serial){
    console.log(serial);
    this.httpservice.removeItem(serial).subscribe(
      (response: any) => {
        this.getItems(); //Calls the fn to retrieve item list
        this.toaster.open("Successfuly deleted.", "Close", {
            duration: 2000,
          });
      },
      (error) => {
        this.toaster.open("Failed to delete.", "Close", {
            duration: 2000,
          });
      }
    )
  }

  postEquipment() {
    document.getElementById("inputEquipment").setAttribute('disabled', 'false');
    if (this.equipment && this.serial && this.specs) {
      let params = {
        'equipment_id': this.equipment,
        'specifications': this.specs,
        'serial': this.serial,
        'post_action': this.postAction
      }

      this.httpservice.addEquipment(params).subscribe(
        (response) => {
          this.toaster.open("Successfuly "+this.msg+".", "Close", {
            duration: 2000,
          });
          this.equipment = '';
          this.serial = '';               //resets the form fields
          this.specs = '';
          JsBarcode('#barcode', "thinklogic");

          this.getItems(); //Calls the fn to retrieve item list
        },
        (error) => {
          this.toaster.open("Failed to "+this.msg+".", "Close", {
            duration: 2000,
          });
        }
      )
    }
    else {
      this.toaster.open("Form not complete.", "Close", {
        duration: 2000,
      });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
