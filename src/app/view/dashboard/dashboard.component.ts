import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientService } from 'src/app/services/httpclient/httpclient.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface DashboardInterface {
  id: string,
  equip_serial_id: string,
  item_condition: string,
  status: string,
  station_id: string,
  created_at: string,
  updated_at: string
}

export interface Serial {
  serial_id: string;
  serial: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  dataSource: any;
  created_at: string;
  updated_at: string;
  userLastLoggedIn: string;
  btnMsg: string;
  station: string;
  stations: any;
  condition: string;
  room: string;
  rooms: any;
  postAction: string;
  postMsg: string;
  equipments: any;
  equipment: string;
  equipSerials: string;
  serials: any;


  displayedColumns: string[] = ['equip_serial_id', 'item_condition', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private httpservice: HttpClientService, private modalService: NgbModal, private toaster: MatSnackBar) {}

  ngOnInit() {
    this.userLastLoggedIn = localStorage.getItem('last_logged_in');

    this.showInventory();       //These calls or invokes the methods declared below to initialize...
    this.showRooms();           
    this.showStations();
    this.getEquipment();
  }

  //Displays the data to the table
  showInventory(){
    this.httpservice.getInventory().subscribe(
      (response: any) => {
        this.dataSource = new MatTableDataSource<DashboardInterface>(response);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  //gets the list of items (serial) for dropdown
  showEquipment(){
    let equipment_id = this.equipment;
    this.httpservice.getEquipSerial(equipment_id).subscribe(
      (response) => {
        this.serials = response;
        console.log(this.serials);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  getEquipment(){
    this.httpservice.getEquipment().subscribe(      
      (response) => {                   //the list of equipments
        this.equipments = response;                       //for the dropdown
      },
      (error) => {
        console.log(error);
      }
    )
  }

  //gets the list of rooms for dropdown
  showRooms(){
    this.httpservice.getRoom().subscribe(
      (response) => {
        this.rooms = response;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  //Displays the station according to the room selected
  showStations() {
    this.station = "";
    if (this.room == "1") {
      let room_id = {
        "room": this.room
      };
      this.httpservice.getStation(room_id).subscribe(
        (response) => {
          this.stations = response;
        },
        (error) => {
          console.log(error);
        }
      )
    } else if (this.room == "2") {
      let room_id = {
        "room": this.room
      };
      this.httpservice.getStation(room_id).subscribe(
        (response) => {
          this.stations = response;
        },
        (error) => {
          console.log(error);
        }
      )
    }
  }

  //Open Add Modal
  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    }); //to open ngModal

    this.btnMsg = "Assign";
    this.postMsg = "assigned";
    this.postAction = "insert";
    this.room = "";
    this.station = "";
    this.equipSerials = "";
    this.condition = "";
  }


  //Open edit modal
  edit(content, data) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    }); //to open ngModal

    console.log(data);

    this.btnMsg = "Update";
    this.postMsg = "updated";
    this.postAction = "update";
    this.room = data.room_id;
    this.station = data.station_id;
    this.equipSerials = data.equip_serial_id;
    this.condition = data.item_condition;
  }

  unassignStation(data){
    let params = {
      "id":data.id,
      "serial_id":data.equip_serial_id
    }
    this.httpservice.unassignStation(params).subscribe(
      (response: any) => {
        this.toaster.open("Successfuly unassigned.", "Close", {
          duration: 2000,
        });
        this.showInventory();
      },
      (error) => {
        this.toaster.open("Failed to unassign.", "Close", {
          duration: 2000,
        });
      }
    )
  }

  //Assign or Edit Equipment in post method
  postEquipment() {
    if (this.room && this.station && this.equipment && this.equipSerials && this.condition) {
      let params = {
        "station_id": this.station,
        //"equip_serial_id": this.equipmentSerials.value.serial_id,
        "equip_serial_id": this.equipSerials,
        "item_condition": this.condition,
        "post_action": this.postAction
      };
      console.log(params);
      this.httpservice.postInventory(params).subscribe(
        (response) => {
          this.toaster.open("Successfuly " + this.postMsg + ".", "Close", {
            duration: 2000,
          });
          this.showInventory();
          this.showEquipment();
          this.room = "";
          this.station = "";
          this.equipment = ""
          this.equipSerials = "";
          this.condition = "";
        },
        (error) => {
          this.toaster.open("Failed to " + this.btnMsg.toLowerCase() + ".", "Close", {
            duration: 2000,
          });
        }
      )
      this.showInventory();
    } else {
      this.toaster.open("Form not complete.", "Close", {
        duration: 2000,
      });
    }
  }


  //Search filter for table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
