import { Component, OnInit } from '@angular/core';
import { HttpClientService } from 'src/app/services/httpclient/httpclient.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
stock;
  constructor(private httpservice : HttpClientService) { }

  getStock(){
    this.httpservice.getStock().subscribe(
      (response)=>{
        this.stock = response;
      },
      (error)=>{
        console.log(error);
      }
    )
  }
  
  ngOnInit() {
  }

}
