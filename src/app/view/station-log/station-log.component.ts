import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { HttpClientService } from '../../services/httpclient/httpclient.service';

export interface StationLogInterface {
  id: string,
  equipment_id: string,
  old_station: string,
  updated_current_station: string,
  date_time_log: string
}

@Component({
  selector: 'app-station-log',
  templateUrl: './station-log.component.html',
  styleUrls: ['./station-log.component.scss']
})
export class StationLogComponent implements OnInit {
	dataSource: any;

	displayedColumns: string[] = ['id', 'equipment_id', 'old_station', 'updated_current_station', 'date_time_log'];
  

	@ViewChild(MatPaginator) paginator: MatPaginator;
 	@ViewChild(MatSort) sort: MatSort;

  constructor(private httpservice: HttpClientService) { }

  ngOnInit() {
  	this.httpservice.getStationLog().subscribe(
      (response:any)=>{
      	this.dataSource = new MatTableDataSource<StationLogInterface>(response);
      	this.dataSource.sort = this.sort;
      	this.dataSource.paginator = this.paginator;
      },
    	(error)=>{
      console.log(error);
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
