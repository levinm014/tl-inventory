import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable()

export class AuthGuardService implements CanActivate {
  constructor(private auth: AuthService, private router: Router) { }
  canActivate(): boolean {
    if (this.auth.isAuthenticated()) {
      //this.router.navigate(['tl',{outlets: {'content':'dashboard'}}]);
      return true;
    }
    else {
      this.router.navigate(['login']);
      return false;
    }
  }
}

// if (this._authService.isLoggedIn) {
//   this.router.navigate(['tl',{outlets: {'content':'dashboard'}}]);
// }